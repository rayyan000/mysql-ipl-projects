# MySQL COMMANDS

### Accessing MySQL Shell
1. mysql –u <username> –p
2. <enter password>

### Creation of databases and users:
CREATE USER 'new_user'@'localhost' 
DROP USER 'OldUser'@'host';
CREATE database IPL-Project-DB;
DROP DATABASE IPL-Project-DB;


### Creation of table
USE database_name;
CREATE TABLE table_name (	/* input correct inputs using csv file headings, for example:
            id INT NOT NULL,
            season VARCHAR(255) NOT NULL,
            date DATE NOT NULL,
            team1 VARCHAR(75),
            team2 VARCHAR(75),
            PRIMARY KEY (id)	like this */
);

### Loading of csv files
Use a loading statement like this, and input your path in it (Ignore the heading row) :

LOAD DATA INFILE '/home/matches.csv'
INTO TABLE matches
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

## MySQL Queries to run IPL Project
Q4) ECONOMIES 2015
SELECT bowler, SUM(total_runs)/((COUNT(bowler)/6) +((MOD(COUNT(bowler),6))/6)) AS economy
FROM deliveries JOIN matches ON deliveries.match_id = matches.id
WHERE matches.season =2015 GROUP BY bowler
ORDER BY economy LIMIT 10;

Q3) EXTRAS 2016
SELECT bowling_team AS BowlingTeam, SUM(extra_runs) AS ExtrasConceded
FROM deliveries  JOIN matches 
ON deliveries.match_id = matches.id
WHERE season = 2016
GROUP BY bowling_team;

Q2) MATCHES WON BY EACH TEAM :
select winner, count(winner)
from matches
group by winner;

Q1) MATCHES PLAYED EACH SEASON
select season, count(season)
from matches
group by season;
