package rxyyxn.jdbc.mysql.ipl;

import java.sql.*;
import java.util.*;

public class Main {
    static final String DATABASE_URL = "jdbc:mysql://localhost/";
    static final String USER = "Rayyan";
    static final String PASSWORD = "rayyan";

    public static void main(String[] args) {
        try{
            Connection connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
            Statement statement = connection.createStatement();
            statement.executeUpdate("USE `IPL-Project-DB`");

            List <Match> matches = getMatchesData(statement);
            List <Delivery> deliveries = getDeliveriesData(statement);

            getMatchesPlayedPerSeason(matches);
            getNumberOfMatchesWonByEachTeam(matches);
            getExtrasConcededByEachTeamIn2016(matches, deliveries);
            get2015TopEconomicalBowlers(matches, deliveries);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void getMatchesPlayedPerSeason(List<Match> matches) {
        HashMap<Integer, Integer> MatchesPerSeason = new HashMap<>();
        for (int index = 1; index < matches.size(); index++)    {
            int CurrentSeason = matches.get(index).getSeason();
            if (!MatchesPerSeason.containsKey(CurrentSeason)) {
                MatchesPerSeason.put(CurrentSeason, 1);
            }   else    {
                int SeasonCount = MatchesPerSeason.get(CurrentSeason);
                MatchesPerSeason.put(CurrentSeason, SeasonCount + 1);
            }
        }
        System.out.println("Matches played per season :");
        System.out.println(MatchesPerSeason);
    }

    private static void getNumberOfMatchesWonByEachTeam(List<Match> matches) {
        HashMap<String, Integer> MatchesWonByEachTeam = new HashMap<>();

        for (int index = 1; index < matches.size(); index++) {

            if (!matches.get(index).getResult().equals("no result")){
                String winner = matches.get(index).getWinner();

                if (!MatchesWonByEachTeam.containsKey(winner)){
                    MatchesWonByEachTeam.put(winner, 1);
                }   else    {
                    int winnerValue = MatchesWonByEachTeam.get(winner);
                    MatchesWonByEachTeam.put(winner, winnerValue+1);
                }
            }

        }
        System.out.println("\nPrinting matches won by each team in IPL: ");
        System.out.println(MatchesWonByEachTeam);
    }

    private static void getExtrasConcededByEachTeamIn2016(List<Match> matches, List<Delivery> deliveries) {
        HashSet<Integer> MatchIDsFor2016 = new HashSet<>();

        for (int index = 1; index < matches.size(); index++) {

            int season = matches.get(index).getSeason();
            if (season == 2016){
                MatchIDsFor2016.add(matches.get(index).getId());
            }

        }

        HashMap<String, Integer> extraRunsConcededPerTeam = new HashMap<>();
        for(int index = 1; index < deliveries.size(); index++){

            if (MatchIDsFor2016.contains(deliveries.get(index).getMatchId()))   {

                int extraRuns = deliveries.get(index).getExtraRuns();
                String bowlingTeam = deliveries.get(index).getBowlingTeam();

                if (!extraRunsConcededPerTeam.containsKey(bowlingTeam)){
                    extraRunsConcededPerTeam.put(bowlingTeam, extraRuns);
                }   else    {
                    extraRunsConcededPerTeam.put(bowlingTeam,extraRunsConcededPerTeam.get(bowlingTeam) + extraRuns);
                }
            }
        }
        System.out.println("\nExtras conceded by each team in 2016 are as follows : ");
        System.out.println(extraRunsConcededPerTeam);
    }

    private static void get2015TopEconomicalBowlers(List<Match> matches, List<Delivery> deliveries) {

        HashSet<Integer> matchID2015 = new HashSet<>();
        int NumberOfBowlersPrinted = 0;
        for(int index = 1; index < matches.size(); index++){
            int season = matches.get(index).getSeason();

            if (season == 2015) {
                matchID2015.add(matches.get(index).getId());
            }
        }

        HashMap<String, Integer> runs = new HashMap<>();
        HashMap<String, Integer> balls = new HashMap<>();

        for(int index = 1; index < deliveries.size(); index++)  {

            if (matchID2015.contains(deliveries.get(index).getMatchId()))   {

                int Runs = deliveries.get(index).getTotalRuns();
                String bowlerName = deliveries.get(index).getBowler();

                if (balls.containsKey(bowlerName))  {
                    int PreviousRuns = balls.get(bowlerName);
                    balls.put(bowlerName, PreviousRuns + 1);
                }   else    {
                    balls.put(bowlerName, 1);
                }

                if(Runs > 0)  {
                    if (!runs.containsKey(deliveries.get(index).getBowler())){
                        runs.put(bowlerName, Runs);
                    }   else    {
                        int previousRuns = runs.get(bowlerName);
                        runs.put(bowlerName, previousRuns + Runs);
                    }
                }
            }
        }

        HashMap<String, Integer> economicalBowlers = new HashMap<>();

        for (String bowlerVsRuns: runs.keySet()) {

            int Runs = runs.get(bowlerVsRuns);
            int Balls = balls.get(bowlerVsRuns);
            economicalBowlers.put(bowlerVsRuns, Runs * 6/Balls);
        }

        List<Map.Entry<String, Integer> > list = new LinkedList<>(economicalBowlers.entrySet());
        list.sort(Map.Entry.comparingByValue());
        HashMap<String, Integer> SortedEconomies = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : list) {
            SortedEconomies.put(entry.getKey(), entry.getValue());
        }
        System.out.println("\nPrinting the most economical bowler names for 2015 : ");
        for(Map.Entry<String, Integer> entry : SortedEconomies.entrySet())  {
            NumberOfBowlersPrinted++;
            System.out.println("Bowler "+ NumberOfBowlersPrinted + " : " + entry.getKey());
            if(NumberOfBowlersPrinted == 10)
                break;
        }
    }

    private static List<Match> getMatchesData(Statement statement){
        ResultSet rs;
        List<Match> matches = new ArrayList<>();
        try {
            rs = statement.executeQuery("SELECT * FROM matches;");
            while (rs.next()){
                int matchID = rs.getInt("id");
                int matchSeason = rs.getInt("season");
                String matchCity = rs.getString("city");
                String matchDate = rs.getString("date");
                String matchTeam1 = rs.getString("team1");
                String matchTeam2 = rs.getString("team2");
                String matchTossWinner = rs.getString("toss_winner");
                String matchTossDecision = rs.getString("toss_decision");
                String matchResult = rs.getString("result");
                int matchDlApplied = rs.getInt("dl_applied");
                String matchWinner = rs.getString("winner");
                int matchWinByRuns = rs.getInt("win_by_runs");
                int matchWinByWickets = rs.getInt("win_by_wickets");
                String matchPlayer = rs.getString("player_of_match");
                String matchVenue = rs.getString("venue");
                String matchUmpire1 = rs.getString("umpire1");
                String matchUmpire2 = rs.getString("umpire2");
                String matchUmpire3 = rs.getString("umpire3");

                Match match = new Match();
                match.setId(matchID);
                match.setSeason(matchSeason);
                match.setCity(matchCity);
                match.setDate(matchDate);
                match.setTeam1(matchTeam1);
                match.setTeam2(matchTeam2);
                match.setTossWinner(matchTossWinner);
                match.setTossDecision(matchTossDecision);
                match.setResult(matchResult);
                match.setDlApplied(matchDlApplied);
                match.setWinner(matchWinner);
                match.setWinByRuns(matchWinByRuns);
                match.setWinByWickets(matchWinByWickets);
                match.setPlayerOFMatch(matchPlayer);
                match.setVenue(matchVenue);
                match.setUmpire1(matchUmpire1);
                match.setUmpire2(matchUmpire2);
                match.setUmpire3(matchUmpire3);
                matches.add(match);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return matches;
    }

    private static List<Delivery> getDeliveriesData(Statement statement){
        ResultSet rs;
        List<Delivery> deliveries = new ArrayList<>();
        try {
            rs = statement.executeQuery("SELECT * FROM deliveries;");
            while (rs.next()) {
                int deliveryID = rs.getInt("match_id");
                int deliveryInning = rs.getInt("inning");
                String deliveryBattingTeam = rs.getString("batting_team");
                String deliveryBowlingTeam = rs.getString("bowling_team");
                int deliveryOvers = rs.getInt("over");
                int deliveryBalls = rs.getInt("ball");
                String deliveryBatsman = rs.getString("batsman");
                String deliveryNonStriker = rs.getString("non_striker");
                String deliveryBowler = rs.getString("bowler");
                int deliveryIsSuperOver = rs.getInt("is_super_over");
                int deliveryWideRuns = rs.getInt("wide_runs");
                int deliveryByeRuns = rs.getInt("bye_runs");
                int deliveryLegbyeRuns = rs.getInt("legbye_runs");
                int deliveryNoBallRuns = rs.getInt("noball_runs");
                int deliveryPenaltyRuns = rs.getInt("penalty_runs");
                int deliveryBatsmanRuns = rs.getInt("batsman_runs");
                int deliveryExtraRuns = rs.getInt("extra_runs");
                int deliveryTotalRuns = rs.getInt("total_runs");
                String deliveryPlayerDismissed = rs.getString("player_dismissed");
                String deliveryDismissalKind = rs.getString("dismissal_kind");
                String deliveryFielder = rs.getString("fielder");

                Delivery delivery = new Delivery();
                delivery.setMatchId(deliveryID);
                delivery.setInning(deliveryInning);
                delivery.setBattingTeam(deliveryBattingTeam);
                delivery.setBowlingTeam(deliveryBowlingTeam);
                delivery.setOver(deliveryOvers);
                delivery.setBall(deliveryBalls);
                delivery.setBatsman(deliveryBatsman);
                delivery.setNonStriker(deliveryNonStriker);
                delivery.setBowler(deliveryBowler);
                delivery.setIsSuperOver(deliveryIsSuperOver);
                delivery.setWideRuns(deliveryWideRuns);
                delivery.setByeRuns(deliveryByeRuns);
                delivery.setLegByeRuns(deliveryLegbyeRuns);
                delivery.setNoBallRuns(deliveryNoBallRuns);
                delivery.setPenaltyRuns(deliveryPenaltyRuns);
                delivery.setBatsmanRuns(deliveryBatsmanRuns);
                delivery.setExtraRuns(deliveryExtraRuns);
                delivery.setTotalRuns(deliveryTotalRuns);
                delivery.setPlayerDismissed(deliveryPlayerDismissed);
                delivery.setDismissalKind(deliveryDismissalKind);
                delivery.setFielder(deliveryFielder);
                deliveries.add(delivery);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return deliveries;
    }
}